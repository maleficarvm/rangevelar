<?php

require_once __DIR__ . "/classes/files.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Range Rover Velar | The Avant-garde Range Rover</title>
    <meta name="keywords" content="Land Rover, Jaguar Land Rover, Range Rover Velar,
    Range Rover, Velar, crossover, SUV, luxury SUV, Range Rover Velar price, Range Rover Velar range,
    Range Rover Velar 0-100, performance, all wheel drive, PHEV, hybrid">
    <meta name="description" content="Range Rover Velar is our most refined and capable medium SUV.
    Explore its advanced driving capabilities and stunning design in detail.">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="True" name="HandheldFriendly">
    <meta id="viewport-tag" name="viewport" content="width=device-width, user-scalable=no,
    minimum-scale=1.0, maximum-scale=1.0, initial-scale=1, minimal-ui, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/site.webmanifest">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#fff8f8">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/mstile.png">
    <link rel="stylesheet" href="/style.css?<?=date('U')?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/scripts.js?<?=date('U')?>"></script>

</head>
<body>

<div id="wrapper">
    <div class="page-container b1">
        <nav>
            <ul class="topmenu">
                <li><a href="#"><b>Range Rover</b></a></li>
                <li><a href="#"><b>Sport</b></a></li>
                <li><a href="#"><b>Velar</b></a></li>
                <li><a href="#"><b>Evoque</b></a></li>
                <li><a href="#"><b>PHEV</b></a></li>
            </ul>
        </nav>
        <nav>
            <ul class="topmenu top-left-menu">
                <li><a href="#"><b>Shop</b></a></li>
                <li><a href="#"><b>Sign in</b></a></li>
                <li><a href="#" id="maskOpener"><b>_</b></a></li>
        </nav>
        <a class="logo" href="#">
            <img src="/img/landrover1.png" alt="image">
        </a>
        <div class="title l1">Land Rover</div><br>
        <div class="title l2">Range Rover Velar</div>
    </div>
    <!--<div class="page-container b2">
    </div>-->
    <!--<div class="page-container info">
    </div>-->
    <div class="page-container b3">
    </div>
    <!--<div class="page-container info2">
    </div>-->
    <div class="page-container b5">
    </div>
    <!--<div class="page-container info2">
    </div>-->
    <div class="page-container b6">
    </div>
    <div class="page-container b7">
    </div>
    <div class="page-container b8">
    </div>
    <div class="page-container b9">
    </div>
    <div class="page-container b10">
    </div>
</div>
    <ul id="leftnav">
        <li class="active"><a goto-screen="0" href="#"><span>NAME1</span></a></li>
        <li><a goto-screen="1" href="#"><span>NAME2</span></a></li>
        <li><a goto-screen="2" href="#"><span>NAME3</span></a></li>
        <li><a goto-screen="3" href="#"><span>NAME4</span></a></li>
        <li><a goto-screen="4" href="#"><span>NAME5</span></a></li>
        <li><a goto-screen="5" href="#"><span>NAME6</span></a></li>
        <li><a goto-screen="6" href="#"><span>NAME7</span></a></li>
        <li><a goto-screen="7" href="#"><span>NAME8</span></a></li>
    </ul>
<div id="menuMask"></div>
<div id="menu">
    <ul>
        <li><a href="#">Menu item</a></li>
        <li><a href="#">Menu item</a></li>
        <li><a href="#">Menu item</a></li>
        <li><a href="#">Menu item</a></li>
        <li><a href="#">Menu item</a></li>
        <li><a href="#">Menu item</a></li>
        <li><a href="#">Menu item</a></li>
        <li><a href="#">Menu item</a></li>
    </ul>
</div>
</body>
</html>